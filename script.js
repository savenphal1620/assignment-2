document.addEventListener("DOMContentLoaded", fetchData);

function fetchData() {
    // Initial data fetch
    showReloadMessage();
    reloadData();
}

function showReloadMessage() {
    // Display the "Reload data..." message
    const reloadMessage = document.getElementById('reloadMessage');
    reloadMessage.style.display = 'block';

    // Hide the message after a delay
    setTimeout(() => {
        reloadMessage.style.display = 'none';
    }, 20000);
}

function reloadData() {
    const apiUrl = "https://qqknevh7guimnafkkwce5ajl6a0rrjxb.lambda-url.us-east-1.on.aws/";

    fetch(apiUrl)
        .then(response => {
            if (!response.ok) {
                throw new Error(`HTTP error! Status: ${response.status}`);
            }
            return response.json();
        })
        .then(data => {
            // Clear existing table data
            clearTable();

            // Display new data
            displayDataInTable(data.data.result.entities);
            console.log('Data:', data.data.result.entities);
        })
        .catch(error => {
            console.log('Fetch error:', error);
        })
        .finally(() => {
            // Hide the "Reload data..." message when the data is loaded
            const reloadMessage = document.getElementById('reloadMessage');
            reloadMessage.style.display = 'none';
        });
}

function clearTable() {
    // Clear existing table data
    const tableBody = document.querySelector('#dataBody');
    tableBody.innerHTML = '';
}

function displayDataInTable(entities) {
    const tableBody = document.querySelector('#dataBody');

    entities.forEach((entity, index) => {
        const row = tableBody.insertRow();
        row.insertCell(0).textContent = index + 1;
        row.insertCell(1).textContent = entity.id;
        row.insertCell(2).textContent = entity.title;
        row.insertCell(3).textContent = entity.cases_count;
        row.insertCell(4).textContent = formatHTMLString(entity.description);
        row.insertCell(5).textContent = entity.created_at;
    });
}

function formatHTMLString(description) {
    const tempElement = document.createElement('div');
    tempElement.innerHTML = description;

    // Retrieve the text content
    const formattedText = tempElement.textContent;

    return description == null ? "null" : formattedText;
}